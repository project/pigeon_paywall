# Pigeon Paywall

This provides integration with the [Pigeon](https://pigeonpaywall.com) content
paywall service from [Sabramedia](https://sabramedia.com).

## Features

* Allows using the Pigeon paywall service to wrap content with a paywall.

## Requirements

Besides Drupal 9 or 10, this module does not have any additional requirements.

## Setup

* Configure the global settings at `/admin/config/services/pigeon-paywall`.
  * Subdomain / account - The hostname used to log into the Pigeon system, e.g
    if the Pigeon admin site is https://pigeon.example.com then the subdomain
    value is "pigeon.example.com".
  * Fingerprint - Provides a layer of extra security to help prevent cookie
    -removal fraud.
  * IDP - If the site being worked on is not
* Add a boolean field to the entity bundle, e.g. content type, that is to be
  protected.
* Use the "Pigeon Paywall controller" field formatter on the field on the "full"
  view mode.
* Configure the field formatter settings as necessary.
* Configure the Pigeon paywall rules to make the site "open" by default.
* Modify the entity template(s) to add the class "pigeon-remove" to control how
  much of the page is to be removed.

With this setup, any nodes that have have the checkbox enabled will require
visitors to have a Pigeon subscription.

## Template setup

The class "pigeon-remove" must be added to the portion of the pages that are to
be hidden for non-subscribers. The "pigeon-context-promotion" class can be
used to provide a promo or teaser of the page's contents.

### Promo wrapper

If a page element is available with the class "pigeon-context-promotion" it
will be shown if the visitor is not a paid member; this DOM structure should be
hidden by default, e.g. using `style="display:none;"`, and it will be made
visible when appropriate.

If a link with the class "pigeon-open" is present in the promo, the visitor
clicking it will open the Pigeon modal.

### Example node.html.twig

    <article{{ attributes }}>
      {% if display_submitted %}
        <footer>
          {{ author_picture }}
          <div{{ author_attributes }}>
            {% trans %}
              Submitted by {{ author_name }} on {{ date }}
            {% endtrans %}
            {{ metadata }}
          </div>
        </footer>
      {% endif %}
      <div class="pigeon-context-promotion" style="display:none;">
      	<p>This page is available to subscribers. <a href="#"
          class="pigeon-open">Sign in to get access</a>.</p>
      </div>
      <div{{ content_attributes.addClass('pigeon-remove') }}>
        {{ content }}
      </div>
    </article>

## Known issues

### Locking field to certain roles using Field Permissions

When using the [Field Permissions
module](https://www.drupal.org/project/field_permissions) to control which
users are able to edit the field that controls the Pigeon Paywall integration,
it is important to make sure that both the "anonymous usre" and "authenticated
user" roles are both given access to view the field, otherwise the paywall will
not load for these roles.


## Suggestions

In order to provide a better visitor experience it may be worth trying out
different options on the page output.

* Add a text field for saving a teaser or summary of the page's contents, then
  include that in the "pigeon-context-promotion" section of the template; make
  sure to exclude that field in the main `{{ content }}` output.

## Credits / contact

Written and maintained by [Damien
McKenna](https://www.drupal.org/u/damienmckenna).

The best way to contact the author is to submit an issue, be it a support
request, a feature request or a bug report, in the [project issue
queue](https://www.drupal.org/project/issues/pigeon_paywall).
