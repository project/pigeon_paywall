<?php

namespace Drupal\pigeon_paywall\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class Config Form.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pigeon_paywall_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'pigeon_paywall.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('pigeon_paywall.settings')->get();

    $form['subdomain'] = [
      '#title' => $this->t('Subdomain / account'),
      '#description' => $this->t('The Pigeon subdomain used during account setup.'),
      '#type' => 'textfield',
      '#default_value' => $config['subdomain'],
      '#required' => TRUE,
    ];

    // Add a link to the admin dashboard.
    if (!empty($config['subdomain'])) {
      $form['subdomain']['#description'] .= ' '
        . $this->t('The <a href="@admin">Pigeon Paywall admin dashboard</a> will be available from this hostname.', [
            '@admin' => 'https://' . $config['subdomain'] . '/admin',
          ]);
    }

    $form['fingerprint'] = [
      '#title' => $this->t('Fingerprint'),
      '#description' => $this->t('True turns on browser fingerprint to reduce cookie removal fraud in soft paywall scenarios.'),
      '#type' => 'checkbox',
      '#default_value' => $config['fingerprint'],
    ];

    $form['idp'] = [
      '#title' => $this->t('IDP'),
      '#description' => $this->t('Enabling this option when pointing to a Pigeon subdomain that is not in the same context of the primary calling domain. For example, if the Pigeon domain is "my.example.com" but this site environment is "dev42.example2.com" then this option must be enabled.'),
      '#type' => 'checkbox',
      '#default_value' => $config['idp'],
    ];

    $form['published_only'] = [
      '#title' => $this->t('Only show on published content'),
      '#description' => $this->t('Enabling this option when pointing to a Pigeon subdomain that is not in the same context of the primary calling domain. For example, if the Pigeon domain is "my.example.com" but this site environment is "dev42.example2.com" then this option must be enabled.'),
      '#type' => 'checkbox',
      '#default_value' => $config['published_only'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Strip unwanted bits off the subdomain string.
    $subdomain = str_replace(['http:', 'https:', '/admin', '/'], '', $form_state->getValue('subdomain'));

    // Retrieve the configuration and save.
    $this->config('pigeon_paywall.settings')
      ->set('subdomain', $subdomain)
      ->set('fingerprint', $form_state->getValue('fingerprint'))
      ->set('idp', $form_state->getValue('idp'))
      ->set('published_only', $form_state->getValue('published_only'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
