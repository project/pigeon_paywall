<?php

namespace Drupal\pigeon_paywall\Plugin\Field\FieldFormatter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation for the Pigeon Paywall formatter.
 *
 * @FieldFormatter(
 *   id = "pigeon_paywall_checkbox",
 *   module = "pigeon_paywall",
 *   label = @Translation("Pigeon Paywall controller"),
 *   field_types = {
 *     "boolean"
 *   }
 * )
 */
class CheckboxPaywall extends FormatterBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ConfigFactoryInterface $config_factory, DateFormatter $date_formatter) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->configFactory = $config_factory;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('config.factory'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [
      $this->t('Controls a paywall on this page.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $paywall = FALSE;

    // Are any of the checkboxes values enabled?
    foreach ($items as $item) {
      if (!empty($item->value)) {
        $paywall = TRUE;
      }
    }

    // Load the paywall logic if requested.
    if ($paywall) {
      // Load the global configuration.
      // @todo Replace with DI.
      $config = $this->configFactory->get('pigeon_paywall.settings');
      // Don't bother doing anything if the subdomain value wasn't set.
      if ($config->get('subdomain') != '') {
        // The entity that this field is attached to.
        /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
        $entity = $items->getEntity();

        // Only show the interface if the entity is published, or if the
        // "published-only" option is not enabled.
        if (!method_exists($entity, 'isPublished')
          || $entity->isPublished()
          || !$config->get('published_only')) {
          return [
            '#attached' => [
              'drupalSettings' => [
                'pigeon' => [
                  'subdomain' => trim($config->get('subdomain')),
                  'fingerprint' => (bool) $config->get('fingerprint'),
                  'idp' => (bool) $config->get('idp'),
                  // Use an ID in the format entitytype:entityid, e.g.
                  // "node:123".
                  'id' => $entity->getEntityTypeId() . ':' . $entity->id(),
                  'title' => $entity->label(),
                  'created' => $this->calculatetime($entity),
                ],
              ],
              'library' => [
                'pigeon_paywall/checkbox_paywall',
              ],
            ],
          ];
        }
      }
    }
    return [];
  }

  /**
   * Format the date & time the content was created.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity to process.
   *
   * @return string
   *   The entity's creation time value in the required format.
   */
  protected function calculatetime(FieldableEntityInterface $entity) {
    // Currently this only supports nodes.
    if (method_exists($entity, 'getCreatedTime')) {
      /** @var \Drupal\node\NodeInterface $entity */
      // The time the entity was created.
      $time = (int) $entity->getCreatedTime();

      // Generate the required output format: 2022-01-01 00:00:00.
      return $this->dateFormatter->format($time, 'custom', 'Y-m-d H:i:s');
    }
    else {
      return '';
    }
  }

}
