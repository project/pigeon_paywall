/**
 * @file
 * Load the Pigeon Paywall system using the settings defined on a field.
 */

(function (Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.pigeon_checkbox_paywall = {
    attach: function(context) {
      if (typeof drupalSettings.pigeon !== 'undefined') {
        var Pigeon = new PigeonClass({
          // The Pigeon subdomain identified during setup.
          subdomain: drupalSettings.pigeon.subdomain,
          // True turns on browser fingerprint to reduce cookie removal fraud in
          // soft paywall scenarios.
          fingerprint: drupalSettings.pigeon.fingerprint,
          // True if subdomain does not share the same primary domain this
          // script is installed on.
          idp: drupalSettings.pigeon.idp
        });
        Pigeon.paywall({
        	// Setting true or false tells Pigeon whether to redirect to  preset
          // promotional page or not. Setting 'modal' tells the page to popup
          // the standard promotional dialog.
          // @todo Make this configurable.
        	redirect: 'modal', 
          // default: false. Setting true overrides the Pigeon Cloud presets,
          // allowing this page to be free.
        	free: false,
          // The response function that is called after the authentication check
          // has completed. See Pigeon Response Reference for available
          // variables. You can use Pigeon.paywallPromise (jQuery promise as
          // seen below) to run code on the callback response elsewhere in your
          // code.
          // success: function(response) {
          // },
          // True doesn't hide the body onload while paywall decides what to do.
          ignoreBodyHide: false,
          // Used in on-demand scenario. The unique id you use for the page or
          // particular product.
          contentId: drupalSettings.pigeon.id,
          // Content title used for identifying content in a nice way.
          contentTitle: drupalSettings.pigeon.title,
          // Content date used for time based access scenarios.
          contentDate: drupalSettings.pigeon.created,
          // Used for the content price on-demand.
          // @todo Make this configurable.
          contentPrice: 0,
          // Used for the content value meter. Used in the credit or
          // voucher-based scenario.
          // @todo Make this configurable.
          contentValue: 0,
          // If true then prompt before taking credit on content value meter.
          contentPrompt: false,
        });
      }
    }
  }

})(Drupal, drupalSettings);
