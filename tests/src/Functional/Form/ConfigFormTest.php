<?php

namespace Drupal\Tests\pigeon_paywall\Functional\Form;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the Drupal\pigeon_paywall\Form\ConfigForm class.
 *
 * @group Form
 */
class ConfigFormTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['pigeon_paywall'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user object for testing the admin interface.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * Perform initial setup tasks that run before every test method.
   */
  public function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
      'administer pigeon paywall',
    ]);
  }

  /**
   * Tests the \Drupal\presspatron\Form\ConfigForm class.
   */
  public function testConfigForm() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/services/pigeon-paywall');
    $this->assertSession()->statusCodeEquals(200);
    $config = $this->config('pigeon_paywall.settings');
    $this->assertSession()->fieldValueEquals(
      'subdomain',
      $config->get('subdomain')
    );
    $this->assertSession()->fieldValueEquals(
      'fingerprint',
      $config->get('fingerprint')
    );
    $this->assertSession()->fieldValueEquals(
      'idp',
      $config->get('idp')
    );
  }

}
